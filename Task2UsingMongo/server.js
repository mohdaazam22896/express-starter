const express = require('express');
const app = express();
const userRouter = require('./router/userRouter');
const config = require('./config');
const db = require('./db');
const error= require('./helper/error');

app.use(error.errorHandler);

app.use('/', userRouter);


db.dbConnect()
.then((res)=>{
    console.log("database connected");
    app.listen(config.PORT, () => {
        console.log("server listen at port no 10000");
    });
})
.catch((err)=>console.log("Something wrong in datanbase "+err));