const express = require('express');
const app = express();
const { getUserById, createUser, updateUser, deleteUser, getAllUsers } = require('../data/userModel');
const User = require('../data/userModel');

class UserController {
    create(req, res, next) {
        const userObject = req.body;
        User.createUser(userObject)
            .then((doc) => res.send("user added"))
            .catch((err) => next(err));
    }

    update(req, res, next) {
        const id = req.params.id;
        const userObject = req.body;
        User.updateUser(id, userObject)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }

    delete(req, res, next) {
        const id = req.params.id;
        User.deleteUser(id)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));

    }
    
    getUsers(req, res, next) {
        User.getAllUsers()
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }

    getUserByIdCtr(req, res) {
        const id = req.params.id;
        User.getUserById(id)
            .then((doc) => res.json(doc))
            .catch((err) => next(err));
    }
}

module.exports = new UserController();