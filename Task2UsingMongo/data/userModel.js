const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
    id: Number,
    firstName: String,
    lastName: String,
    email: { type: String, lowercase: true, trim: true, unique: true },
});

User.statics.getAllUsers = function () {
    return this.find();
}

User.statics.updateUser = function (id, user) {
    return this.findOneAndUpdate({ id: id }, { $set: user }, { upsert: true, new: true });
}

User.statics.deleteUser = function (id) {
    return this.findOneAndRemove({ id: id });
}


User.statics.getUserById = function (id) {
    console.log(id)
    return this.findOne({ id: id });
}

User.statics.createUser = function (user) {
    let data = new this(user);
    return data.save();
}

module.exports = mongoose.model('User', User);