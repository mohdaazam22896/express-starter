const config = require('./config');
const mongoose = require('mongoose');


function dbConnect() {
    return mongoose.connect(config.dbURL,{ useNewUrlParser: true });
}

module.exports = {
    dbConnect:dbConnect
}