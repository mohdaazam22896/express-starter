const PORT = '10000';
const dbURL = 'mongodb://localhost:27017/userdb';

module.exports = {
    PORT: PORT,
    dbURL: dbURL
}