const express = require('express');
const router = express.Router();
const userController = require('../controller/users');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/users/:id', userController.getUserByIdCtr);

router.get('/users', userController.getUsers);

router.delete('/users/:id', userController.delete);

router.post('/users', userController.create);

router.put('/users/:id', userController.update);

module.exports = router;