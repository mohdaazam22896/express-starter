const express = require('express');
const app = express();
const userRouter = require('./router/userRouter');

app.use('/', userRouter);

app.listen(10000, () => {
    console.log("server listen at port no 10000");
});