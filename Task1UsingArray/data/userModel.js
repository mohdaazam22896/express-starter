let database = [
    {
        id: 1,
        firstname: "mohd",
        lastname: "aazam",
        "email": "mohdaazam@gmail.com"
    },
    {
        id: 2,
        firstname: "manish",
        lastname: "kumar",
        email: "manish@gmail.com"
    },
    {
        id: 3,
        firstname: "mohd",
        lastname: "aazam",
        email: "mohdaazam@gmail.com"
    },
    {
        id: 4,
        firstname: "mohd",
        lastname: "aazam",
        email: "mohdaazam@gmail.com"
    }
]


const getUserById = (id,res) => {
    const index = getIndex(id);
    const object = database[index];
    console.log(object);
    res.send(object);
}

const createUser = (object,res) => {
    database.push(object);
    res.send("data successfully stored")
}


const updateUser = (object, id,res) => {
    const index = getIndex(id);
    database[userIndex] = object;
    res.send("User successfully updated");
}

const deleteUser = (id,res) => {
    let index = getIndex(id);
    database.splice(index, id);
    res.send("user id "+id+" sucessfully delete");
}

const checkUserExist = (id) => {
    let isFound = false;
    database.forEach(element => {
        if (element.id == id) {
            isFound = true;
        }
    });
    return isFound;
}

const getIndex = (id) => {
    let userIndex;
    database.forEach((object, index) => {
        if (object.id == id) {
            userIndex =  index;
        }
    });
    return userIndex;
}

const getAllUsers= (res)=>{
    res.send(database);
}

module.exports = {
    getUserById: getUserById,
    createUser: createUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
    getAllUsers:getAllUsers,
    checkUserExist: checkUserExist
}





