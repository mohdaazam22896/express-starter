const express =require('express');
const app = express();
const {getUserById,createUser,updateUser,deleteUser,getAllUsers} = require('../data/userModel');

class UserController{
    create(req,res){  
        createUser(req.body,res);
    }

    update(req,res){
         updateUser(req.body,req.params.id,res);
    }

    delete(req,res){
          deleteUser(req.params.id,res);
    }

    getUsers(req,res){
       getAllUsers(res);
    }

    getUserByIdCtr(req,res){
     
      getUserById(req.params.id,res);
    }
}

module.exports = new UserController();