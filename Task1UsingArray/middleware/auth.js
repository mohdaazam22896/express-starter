const userModel = require('../data/userModel');
const helper = require('../helper/error');

const middleware = (req, res, next) => {
    if (req.method == 'GET') {
        if (userModel.checkUserExist(req.params.id)) {
            next();
        } else {
            res.json(helper.userNotFoundError);
        }
    } else if (req.method == 'POST') {
        if (!userModel.checkUserExist(req.body.id)) {
            next();
        } else {
            res.json(helper.userAlreadyExistError)
        }
    } else if (req.method == 'DELETE') {
        if (userModel.checkUserExist(req.params.id)) {
            next();
        } else {
            res.json(helper.userNotDeleteError);
        }
    } else if (req.method == 'PUT') {
        if (userModel.checkUserExist(req.params.id)) {
            if (!userModel.checkUserExist(req.body.id)) {
                next()
            } else {
                res.json(helper.userAlreadyExistError);
            }
        } else {
            res.json(helper.userNotFoundError);
        }
    }

}
module.exports = middleware;
