const express = require('express');
const router = express.Router();
const userController = require('../controller/users');
const middleware = require('../middleware/auth');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

router.get('/users/:id', middleware, userController.getUserByIdCtr);

router.get('/users', userController.getUsers);

router.delete('/users/:id', middleware, userController.delete);

router.post('/users', middleware, userController.create);

router.put('/users/:id', middleware, userController.update);

module.exports = router;