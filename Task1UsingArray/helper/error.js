
const userNotFoundError = {
    status: "Error",
    message: "Sorry, the user you were looking for is not found or deleted."
}

const userAlreadyExistError = {
    status: "Error",
    message: "Sorry, User already Exist."
}
const userNotDeleteError = {
    status: "Error",
    message: "Sorry, User not Exist."
}

module.exports = {
    userNotFoundError: userNotFoundError,
    userAlreadyExistError: userAlreadyExistError,
    userNotDeleteError:userNotDeleteError
} 