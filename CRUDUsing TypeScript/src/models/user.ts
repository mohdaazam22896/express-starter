import * as mongoose from 'mongoose';
import { Model, Schema } from 'mongoose';
import auth from '../middleware/auth';
import { IUser, IUserModel } from './interface';

/**
 * @swagger
 * definitions:
 *   User:
 *     properties:
 *       name:
 *         type: string
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 */

const User = new Schema({
  name: { type: String, required: true },
  username: { type: String, required: true, unique: true },
  email: { type: String, lowercase: true, trim: true, unique: true, required: true },
  password: { type: String, required: true },
});

/**
 * function getAllUsers get all users from database
 */
User.statics.getUsers = function (id: string): Promise<IUser> {
  // console.log(id);
  let query;
  if (id === undefined) {
    query = {};
  } else {
    query = { _id: id };
  }
  return this.find(query, { __v: 0 });
};

/**
 * function updateUser update existing user by id in database
 */
User.statics.updateUser = function (id: string, user: object): Promise<IUser> {
  return this.findOneAndUpdate({ _id: id }, { $set: { name: user['name'] } }, { upsert: true, new: true });
};

/**
 * function deleteUser delete  user from database by id
 */
User.statics.deleteUser = function (id: string): Promise<IUser> {
  return this.findOneAndRemove({ _id: id }, { __v: 0 });
};

/**
 * function getUserById get user from database by id
 */
// User.statics.getUserById = function (id: number): Promise<IUser> {
//   return this.findOne({ _id: id }, { __v: 0 });
// };

/**
 * function checkUserExist check user exist in database
 */
User.statics.checkUserExist = function (username: string): Promise<IUser> {
  return this.findOne({ username }, { __v: 0 });
};

/**
 * function createUser save user details in database
 */
User.statics.createUser = function (user: IUser): Promise<IUser> {
  const { username, password, email, name } = user;
  const hashPassword = auth.craeteHash(password);
  return new this({ password: hashPassword, username, email, name }).save();
};

const UserModel: IUserModel = mongoose.model<IUser, IUserModel>('User', User);
export default UserModel;