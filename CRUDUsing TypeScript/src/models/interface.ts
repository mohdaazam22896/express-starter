import { Document, Model } from 'mongoose';

export interface IUser extends Document {
    username: string;
    password: string;
    email: string;
    name: string;
}

export interface IUserModel extends Model<IUser> {
    getUsers(id: string): Promise<IUser>;
    createUser(user: IUser): Promise<IUser>;
    updateUser(id: string, user: IUser): Promise<IUser>;
    deleteUser(id: string): Promise<IUser>;
    // getUserById(id: number): Promise<IUser>;
    checkUserExist(username: string): Promise<IUser>;

}