class SuccessHandler {

/**
 * @swagger
 * definitions:
 *   Success:
 *     properties:
 *       status:
 *         type: string
 *       data:
 *         type: string
 */

  /**
   * function success used for common success response
   * @param req
   * @param res
   * @param next
   * @param data
   */
  public success(req, res, next, data): any {
    res.status(200).json({ status: res.statusCode, data: data });
  }
}

export default SuccessHandler;