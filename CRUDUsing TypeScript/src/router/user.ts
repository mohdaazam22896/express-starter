import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import { UserController } from '../controller/user';
import auth from '../middleware/auth';
import validation from '../validation/user';

const router = Router();
const userCtroller: UserController = new UserController();

// router.route('/login')
//     .post(auth.createToken);

// router.route('/users')
//     .get(checkSchema(validation.create as any), userCtroller.getUsers)
//     .post(userCtroller.create);

// router.route('/users/:id')
//     .get(auth.verifyToken, userCtroller.getUserById)
//     .put(auth.verifyToken, userCtroller.update)
//     .delete(userCtroller.delete);

/**
 * @swagger
 * definitions:
 *   Login:
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 */

/**
 * @swagger
 * /api/login:
 *   post:
 *     tags:
 *       - Login
 *     description: Login to craete new token
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: Login to create new unique token
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Login'
 *     responses:
 *       200:
 *         description: Successfully created
 *       500:
 *         description: Error! Internal Server Error
 *         schema:
 *           $ref: '#/definitions/Error'
 */

router.post('/login', checkSchema(validation.login as any), auth.createToken);

/**
 * @swagger
 * /api/users:
 *   post:
 *     tags:
 *       - User
 *     description: create new user in system
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: create new user
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Successfully created
 *       500:
 *         description: Error! Internal Server Error
 *         schema:
 *           $ref: '#/definitions/Error'
 *   get:
 *     tags:
 *       - User
 *     description: Get All User from the database
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: All User Successfully get from the database
 */

router.get('/users(/:id)?', auth.verifyToken, userCtroller.getUsers);
router.post('/users', checkSchema(validation.create as any), userCtroller.create);

/**
 * @swagger
 * /api/users/{Id}:
 *   delete:
 *     tags:
 *       - User
 *     description: Delete User by ID
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Id
 *         description: ID of user that we want to delete
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully deleted
 *         schema:
 *           $ref: '#/definitions/Success'
 *       500:
 *         description: Error! Internal Server Error
 *         schema:
 *           $ref: '#/definitions/Error'
 *   get:
 *     tags:
 *       - User
 *     description: Get User by ID
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: Id
 *         description: ID of user that we want to get
 *         in: path
 *         required: true
 *         type: string
 *       - name: Authorization
 *         description: Access token
 *         in: headers
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Successfully get
 *         schema:
 *           $ref: '#/definitions/Success'
 *       500:
 *         description: Error! Internal Server Error
 *         schema:
 *           $ref: '#/definitions/Error'
 */

// router.get('/users/:id', auth.verifyToken, userCtroller.getUserById);
router.delete('/users/:id', userCtroller.delete);
router.put('/users/:id', auth.verifyToken, userCtroller.update);

export default router;
