import { Router } from 'express';
const app = Router();
import router from './router/user';

app.use('/api', router);
export default app;