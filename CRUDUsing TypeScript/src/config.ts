export class Config {
  public secretKey = 'ilovecoding';
  public PORT: number = 10000;
  public dbURL: string = 'mongodb://localhost:27017/userdb';
}
