import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';
import SuccessHandler from '../helper/success';
import { IUser } from '../models/interface';
import UserModel from '../models/user';

const successHandler: SuccessHandler = new SuccessHandler();

export class UserController {

  /**
   * function getUsers used for handle the request and response
   * @param req
   * @param res
   * @param next
   */

  public getUsers(req: Request, res: Response, next: NextFunction): void {
    // console.log(req.params.id);
    const id: string = req.params.id;
    UserModel.getUsers(id)
      .then((doc: IUser) => {
        if (doc[0]) {
          successHandler.success(req, res, next, doc);
        } else {
          res.status(404).json({ status: 'Error', message: 'User not found' });
        }
      })
      .catch((err: Error) => res.status(400).json({ type: err.name, message: err.message }));
  }

  /**
   * function create used for handle the request and response
   * @param req
   * @param res
   * @param next
   */
  public create(req: Request, res: Response, next: NextFunction): void {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.log(errors.array());
      res.status(500).json({ status: 'error', errors: errors.array() });
    } else {
      UserModel.createUser(req.body)
        .then((doc: IUser) => successHandler.success(req, res, next, doc._id))
        .catch((err: Error) => res.status(409).json({ type: err.name, message: err.message }));
    }
  }

  /**
 * function update used for handle the request and response
 * @param req
 * @param res
 * @param next
 */
  public update(req: Request, res: Response, next: NextFunction): void {
    const id: string = req.params.id;
    const userObject: IUser = req.body;
    console.log(id, userObject);
    UserModel.updateUser(id, userObject)
      .then((doc: object) => successHandler.success(req, res, next, id))
      .catch((err: Error) => res.status(400).json({ type: err.name, message: err.message }));
  }

  /**
 * function delete used for handle the request and response
 * @param req
 * @param res
 * @param next
 */
  public delete(req: Request, res: Response, next: NextFunction): void {
    const id: string = req.params.id;
    UserModel.deleteUser(id)
      .then((doc: IUser) => {
        if (doc[0]) {
          successHandler.success(req, res, next, null);
        } else {
          res.status(404).json({ status: 'Error', message: 'User not found' });
        }
      })
      .catch((err: Error) => res.status(400).json({ type: err.name, message: err.message }));
  }

  /**
  * function getUserById used for handle the request and response
  * @param req
  * @param res
  * @param next
  */
  // public getUserById(req: Request, res: Response, next: NextFunction): void {
  //   const id: number = req.params.id;
  //   UserModel.getUserById(id)
  //     .then((doc: IUser) => successHandler.success(req, res, next, doc))
  //     .catch((err: Error) => res.json({ type: err.name, message: err.message }));
  // }

}