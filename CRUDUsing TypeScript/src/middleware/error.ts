class ErrorHandler {

  /**
   * @swagger
   * definitions:
   *   Error:
   *     properties:
   *       status:
   *         type: string
   *       errors:
   *         type: object
   */

  /**
   * @param err
   * @param req
   * @param res
   * @param next
   */
  errorHandler(err, req, res, next): void {
    res.status(500);
    res.json('error', {
      message: err.message,
      error: {},
    });
  }
}

export default new ErrorHandler();