import { NextFunction, Request, Response } from 'express';
import UserModel from '../models/user';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
import { validationResult } from 'express-validator/check';
import { Config } from '../config';
const secretKey = new Config().secretKey;
const saltRounds = 10;

class Authentication {
  /**
   * function createToken used for create the unique token
   * @param req
   * @param res
   * @param next
   */
  public createToken(req: Request, res: Response, next: NextFunction): any {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(500).json({ status: 'error', errors: errors.array() });
    } else {
      const { username, password } = req.body;
      UserModel.checkUserExist(username)
        .then((doc) => {
          if (doc) {
            bcrypt.compare(password, doc.password)
              .then((result) => {
                if (result) {
                  jwt.sign({ username, password }, secretKey, (err, token) => {
                    console.log(doc);
                    res.json({
                      token,
                    });
                  });
                } else {
                  res.json({ status: 'Error', message: 'User not authenticate for token' });

                }
              });

          } else {
            res.json({ status: 'Error', message: 'Username not Exist' });
          }
        })
        .catch((err) => {
          res.sendStatus(403);
        });
    }
  }

  /**
   * function verifyToken used for verify the token
   * @param req
   * @param res
   * @param next
   */
  public verifyToken(req: Request, res: Response, next: NextFunction) {
    const token = req.headers['authorization'];
    console.log(token);
    if (typeof token !== 'undefined') {
      jwt.verify(token, secretKey, (err, data) => {
        if (!err) {
          next();
        } else {
          res.sendStatus(403);
        }
      });
    } else {
      res.sendStatus(403);
    }
  }

  /**
   * function craeteHash create the hash value of password
   * @param password {string}
   */
  public craeteHash(password: string) {
    const hash = bcrypt.hashSync(password, saltRounds);
    return hash;
  }
}

export default new Authentication();