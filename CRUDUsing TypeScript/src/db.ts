import * as mongoose from 'mongoose';
import { Config } from './config';

export class DatabaseConfig {
  public dbConncet(): Promise<mongoose.Mongoose> {
    const dbURL: string = new Config().dbURL;
    return mongoose.connect(dbURL, { useNewUrlParser: true });
  }
}