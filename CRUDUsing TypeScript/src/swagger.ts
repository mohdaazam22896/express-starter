const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
  info: {
    title: 'Node Swagger API',
    version: '1.0.0',
    description: 'Node Swagger Rest API Documente',
  },
  host: 'localhost:10000',
  basePath: '/',
};
const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./**/dist/**/*.js'],
};
const swaggerSpec = swaggerJSDoc(options);
export default swaggerSpec;