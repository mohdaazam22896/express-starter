export default {
  create: {
    username: {
      in: ['body'],
      matches: {
        errorMessage: 'Username contains only alphabets and minimum 8 letters',
        options: [/[a-zA-Z0-9]{8,}/, 'g'],
      },
    },
    password: {
      in: ['body'],
      matches: {
        errorMessage: 'Password must have Minimum eight characters, at least one letter and one number',
        options: [/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'g'],
      },
    },
    email: {
      in: ['body'],
      matches: {
        errorMessage: 'Email is wrong',
        options: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'g'],
      },
    },
    name: {
      in: ['body'],
      isLength: {
        errorMessage: 'Name should be at least 5 chars',
        options: { min: 5 },
      },
    },
  },
  login: {
    username: {
      in: ['body'],
      matches: {
        errorMessage: 'Username contains only alphabets and minimum 8 letters',
        options: [/[a-zA-Z]{8,}/, 'g'],
      },
    },
    password: {
      in: ['body'],
      matches: {
        errorMessage: 'Password must have Minimum eight characters, at least one letter and one number',
        options: [/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'g'],
      },
    },
  },
};
