import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as http from 'http';
import { Config } from './config';
import { DatabaseConfig } from './db';
import error from './middleware/error';
import router from './router';
import swaggerSpec from './swagger';
const swaggerUi = require('swagger-ui-express');
const databaseConfig: DatabaseConfig = new DatabaseConfig();

class Server {

  public app: express.Application;
  public port: number = new Config().PORT;
  public server: http.Server;

  public constructor() {
    this.app = express();
    this._connect();
    this.app.set('secret', new Config().secretKey);
    this.app.use(error.errorHandler);
    this.app.use('/', router);
    this.app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
  }

  private _connect(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    databaseConfig.dbConncet()
      .then((res) => {
        this.app.listen(this.port, () => {
          console.log('Database Connected');
          console.log('Express server listening on port ' + this.port);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

}

const obj = new Server();
const app = obj.app;
const server = obj.server;
module.exports = {
  app,
  server,
};
// new Server().app;