const request = require('supertest');

describe('test suit for Rest APIs', () => {
    let server;
    let app;

    beforeAll(() => {
        console.log('server initialize');
        const obj = require('../src/server');
        server = obj.server;
        app = obj.app;
    });

    // describe('test suit for POST Request', () => {
    //     const users = require('./mock_data').users;
    //     users.forEach((mockdata) => {
    //         it('Post Request', (done) => {
    //             request(app)
    //                 .post('/api/users')
    //                 .send(mockdata)
    //                 .end((err, response) => {
    //                     expect(response.type).toBe('application/json');
    //                     expect(response.status).toBe(200);
    //                     done();
    //                 });
    //         });

    //         it('Post Request for Duplicate key error ', (done) => {
    //             request(app)
    //                 .post('/api/users')
    //                 .send(mockdata)
    //                 .end((err, response) => {
    //                     expect(response.type).toBe('application/json');
    //                     expect(response.status).toBe(409);
    //                     done();
    //                 });
    //         });

    //     });

    // });

    // describe('test suit for GET Request', function () {

    //     it('GET Request by ObjectId failed', (done) => {
    //         const id = '5b926db754681229ffc4c461-1';
    //         request(app)
    //             .get('/api/users/' + id)
    //             .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
    //             .end((err, response) => {
    //                 expect(response.status).toBe(400);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });

    //     it('GET Request by correct id', (done) => {
    //         const id = '5b96595deaf0d2602f998a4f';
    //         request(app)
    //             .get('/api/users/' + id)
    //             .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
    //             .end((err, response) => {
    //                 expect(response.status).toBe(200);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });

    //     it('GET Request by incorrect id', (done) => {
    //         const id = '5b926db754681229ffc4c461';
    //         request(app)
    //             .get('/api/users/' + id)
    //             .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
    //             .end((err, response) => {
    //                 expect(response.status).toBe(404);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });

    //     it('GET Request for all users with correct token', (done) => {
    //         request(app)
    //             .get('/api/users/')
    //             .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
    //             .end((err, response) => {
    //                 expect(response.type).toBe('application/json');
    //                 expect(response.status).toBe(200);
    //                 done();
    //             });
    //     });

    //     it('GET Request for all users with incorrect token', (done) => {
    //         const id = '5b926db754681229ffc4c461';
    //         request(app)
    //             .get('/api/users/')
    //             .set('Authorization', 'fyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
    //             .end((err, response) => {
    //                 expect(response.status).toBe(403);
    //                 done();
    //             });
    //     });
    // });

    describe('test suit for PUT Request', () => {

        it('PUT Request with incorrect id', (done) => {
            const id: string = '5b97452a2e735519430be5251';
            request(app)
                .put('/api/users/' + id)
                .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
                .send({ name: 'Mohd Aazam khan' })
                .end((err, response) => {
                    expect(response.status).not.toBe(200);
                    expect(response.type).toBe('application/json');
                    done();
                });
        });

        it('PUT Request with correct token', (done) => {
            const id: string = '5b97452a2e735519430be525';
            request(app)
                .put('/api/users/' + id)
                .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
                .send({ name: 'Mohd Aazam khan' })
                .end((err, response) => {
                    expect(response.status).toBe(200);
                    expect(response.type).toBe('application/json');
                    done();
                });
        });

        it('PUT Request with incorrect token', (done) => {
            const id: string = '5b97452a2e735519430be525';
            request(app)
                .put('/api/users/' + id)
                .set('Authorization', 'fyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im1vaGRhYXphbSIsInBhc3N3b3JkIjoibW9oZDEyMzQiLCJpYXQiOjE1MzYyOTQ3MDZ9.wpKC5YFRR_mkAnlIuiK0B6wdlhGhzUI5uZ9oT9cq9NA')
                .send({ name: 'Mohd Aazam' })
                .end((err, response) => {
                    expect(response.status).not.toBe(200);
                    done();
                });
        });

    });

    // describe('test suit for DELETE Request', () => {

    //     it('Delete Request', (done) => {
    //         const id: string = '5b96595deaf0d2602f998a4f';
    //         request(app)
    //             .delete('/api/users/' + id)
    //             .end((err, response) => {
    //                 console.log(response.body);
    //                 expect(response.status).toBe(200);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });

    //     it('Delete Request when user not available in db ', (done) => {
    //         const id: string = '5b96595deaf0d2602f998a4f';
    //         request(app)
    //             .delete('/api/users/' + id)
    //             .end((err, response) => {
    //                 console.log(response.body);
    //                 expect(response.status).toBe(404);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });

    //     it('Delete Request when user Enter ObjectId', (done) => {
    //         const id: string = '5b96595deaf0d2602f998a4f453';
    //         request(app)
    //             .delete('/api/users/' + id)
    //             .end((err, response) => {
    //                 console.log(response.body);
    //                 expect(response.status).toBe(404);
    //                 expect(response.type).toBe('application/json');
    //                 done();
    //             });
    //     });
    // });

    afterAll(() => {
        console.log('server closed...');
        server.close();
    });
});