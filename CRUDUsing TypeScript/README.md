# express-starter-typescript
# Overview
Express starter, is a basic application from where a developer can start a new REST API service. It showcase basic feature of an API with typescript in express.

# Technology Stack
 * [Node.js](https://nodejs.org) server.
 * [MongoDB](https://docs.mongodb.com/) database.
 * [mongoose](http://mongoosejs.com/) Mongoose provides a straight-forward, schema-based solution to model your application data. It includes built-in type casting, validation, query building, business logic hooks and more.
 * [TypeScript](https://www.typescriptlang.org/docs/home.html)
 * [Express](https://expressjs.com/) node.js framework for building REST APIs
 * [Jest](https://jestjs.io/docs/en/getting-started) Jest is used to test Javascript frameworks
 * [Supertest](https://www.npmjs.com/package/supertest) provide a high-level abstraction for testing HTTP
 * [Swagger](https://swagger.io/docs/specification/about/) Swagger is a set of open-source tools built around the OpenAPI Specification that can help you design, build, document and consume REST APIs


# Getting Started - Setup

This section is for getting started with Kue server on your development environment.

1. **Clone the repository**
  ```
  git clone https://mohdaazam22896@bitbucket.org/mohdaazam22896/express-starter.git
  ```
2. **Install packages**
  ```
  npm install
  ```

3. **Launch the app in watch mode**
  ```
  npm start
  ```
Now you can expect automatic live reloading whenever you made changes to ```src```

4. **Build the app**
```
npm build
```
You can access the api at http://localhost:10000/

# Testing 
We use unit tests with [Jest](https://github.com/facebook/jest) in this project.

- To run tests

  ```
  npm test
  ```

# Linting
We also use [tslint](https://palantir.github.io/tslint/) with Typescript Standard Style.

- To run lint:

  ```
  npm lint
  ```

- To automatically fix doable linting errors:

  ```
  npm lint:fix
  ```


# Workflow

#### Development Category
For commit message or branch name we will use following categories:
  - feat (feature)
  - fix (bug fix)
  - docs (documentation)
  - style (formatting, missing semi colons, etc)
  - refactor
  - test (when adding missing tests)
  - chore (maintain)

#### Format of Commit Message

```
  <DEVELOPMENT CATEGORY>: <SUBJECT>
  <BLANK LINE>
  <BODY>
  <BLANK LINE>
  <FOOTER>
```

### IDE
Our preferred IDE is `VSCODE`

Please enable following plugins for your editor:
- **EditorConfig:** To enable reading of .editorconfig file for consistent coding convention.
- **TSlint:** For linting errors
